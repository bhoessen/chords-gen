A simple html/js project to generate music and chords from javascript.

To see the project live, go to https://bhoessen.gitlab.io/chords-gen/

This project is under the GNU GPL v3 licence.
